Change-Log
===============
> Regular configuration update: _30.05.2021_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 3.x ##

### [3.0.0](https://bitbucket.org/android-universum/utils/wiki/version/3.x) ###
> 26.06.2021

- Refactored and changed `Logger` into interface and added `SimpleLogger` implementation.

## [Version 2.x](https://bitbucket.org/android-universum/utils/wiki/version/2.x) ##
## [Version 1.x](https://bitbucket.org/android-universum/utils/wiki/version/1.x) ##