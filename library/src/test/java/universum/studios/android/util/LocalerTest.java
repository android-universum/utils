/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.content.res.Resources;

import org.junit.Test;

import java.util.Locale;

import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class LocalerTest extends AndroidTestCase {

	@Test public void testInstantiation() {
		// Act:
		final Localer localer = new Localer();

		// Assert:
		assertThat(localer.getLocale(), is(Locale.ENGLISH));
	}

	@Test public void testInstantiationWithLocale() {
		// Act:
		final Localer localer = new Localer(Locale.FRENCH);

		// Assert:
		assertThat(localer.getLocale(), is(Locale.FRENCH));
	}

	@Test public void testDispatchApplicationCreated() {
		// Arrange:
		final Resources resources = getContext().getResources();
		final Localer localer = new Localer(Locale.FRENCH);

		// Act:
		localer.dispatchApplicationCreated(resources);

		// Assert:
		assertThat(Locale.getDefault(), is(Locale.FRENCH));
		assertThat(resources.getConfiguration().locale, is(Locale.FRENCH));
	}

	@Test public void testDispatchConfigurationChanged() {
		// Arrange:
		final Resources resources = getContext().getResources();
		final Localer localer = new Localer(Locale.FRENCH);

		// Act:
		localer.dispatchConfigurationChanged(resources.getConfiguration(), resources);

		// Assert:
		assertThat(Locale.getDefault(), is(Locale.FRENCH));
		assertThat(resources.getConfiguration().locale, is(Locale.FRENCH));
	}

	@Test public void testLocale() {
		// Arrange:
		final Resources resources = getContext().getResources();
		final Localer localer = new Localer();

		// Act + Assert:
		localer.setLocale(Locale.FRENCH, resources);
		assertThat(Locale.getDefault(), is(Locale.FRENCH));
		assertThat(resources.getConfiguration().locale, is(Locale.FRENCH));
	}
}