/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import org.junit.Test;

import universum.studios.android.testing.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class FlagsTest implements TestCase {

	@Test public void testInstantiation() {
		// Act:
		final Flags flags = new Flags();

		// Assert:
		assertThat(flags.get(), is(0));
	}

	@Test public void testInstantiationWithInitialValue() {
		// Act:
		final Flags flags = new Flags(0x00000001 << 2);

		// Assert:
		assertThat(flags.get(), is(0x00000001 << 2));
	}

	@Test public void testAdd() {
		// Arrange:
		final Flags flags = new Flags();

		// Act + Assert:
		assertThat(flags.add(0x00000001 << 1), is(flags));
		assertThat(flags.get(), is(0x00000001 << 1));
		assertThat(flags.has(0x00000001 << 1), is(true));
		assertThat(flags.add(0x00000001 << 2), is(flags));
		assertThat(flags.get(), is(0x00000001 << 1 | 0x00000001 << 2));
		assertThat(flags.has(0x00000001 << 2), is(true));
		assertThat(flags.add(0x00000001 << 3), is(flags));
		assertThat(flags.get(), is(0x00000001 << 1 | 0x00000001 << 2 | 0x00000001 << 3));
		assertThat(flags.has(0x00000001 << 3), is(true));
		assertThat(flags.has(0x00000001 << 4), is(false));
	}

	@Test public void testRemove() {
		// Arrange:
		final Flags flags = new Flags(0x00000001 << 1 | 0x00000001 << 2 | 0x00000001 << 3);

		// Act + Assert:
		assertThat(flags.remove(0x00000001 << 1), is(flags));
		assertThat(flags.get(), is(0x00000001 << 2 | 0x00000001 << 3));
		assertThat(flags.has(0x00000001 << 1), is(false));
		assertThat(flags.remove(0x00000001 << 2), is(flags));
		assertThat(flags.get(), is(0x00000001 << 3));
		assertThat(flags.has(0x00000001 << 2), is(false));
		assertThat(flags.remove(0x00000001 << 3), is(flags));
		assertThat(flags.get(), is(0));
		assertThat(flags.has(0x00000001 << 3), is(false));
	}

	@Test public void testReset() {
		// Arrange:
		final Flags flags = new Flags(8);

		// Act:
		assertThat(flags.reset(), is(flags));

		// Assert:
		assertThat(flags.get(), is(0));
	}
}