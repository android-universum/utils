/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.testing.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class BundleKeyTest implements TestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		BundleKey.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<BundleKey> constructor = BundleKey.class.getDeclaredConstructor();
		constructor.setAccessible(true);

		// Act:
		constructor.newInstance();
	}

	@Test public void testBasic() {
		// Act + Assert:
		assertThat(
				BundleKey.basic(FirstBundleClass.class, "Param.1"),
				is(FirstBundleClass.class.getName() + ".KEY.Param.1")
		);
		assertThat(
				BundleKey.basic(SecondBundleClass.class, "Param.2"),
				is(SecondBundleClass.class.getName() + ".KEY.Param.2")
		);
	}

	@Test public void testArgument() {
		// Act + Assert:
		assertThat(
				BundleKey.argument(FirstBundleClass.class, "Arg.1"),
				is(FirstBundleClass.class.getName() + ".ARGUMENT.Arg.1")
		);
		assertThat(
				BundleKey.argument(SecondBundleClass.class, "Arg.2"),
				is(SecondBundleClass.class.getName() + ".ARGUMENT.Arg.2")
		);
	}

	@Test public void testSavedState() {
		// Act + Assert:
		assertThat(
				BundleKey.savedState(FirstBundleClass.class, "State.1"),
				is(FirstBundleClass.class.getName() + ".SAVED_STATE.State.1")
		);
		assertThat(
				BundleKey.savedState(SecondBundleClass.class, "State.2"),
				is(SecondBundleClass.class.getName() + ".SAVED_STATE.State.2")
		);
	}

	private static final class FirstBundleClass {}

	private static final class SecondBundleClass {}
}