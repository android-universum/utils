/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.Manifest;
import android.content.pm.PackageManager;

import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.testing.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class PermissionsTest extends AndroidTestCase {

	private static final String INTERNET = Manifest.permission.INTERNET;
	private static final String WAKE_LOCK = Manifest.permission.WAKE_LOCK;
	private static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
	private static final String READ_CONTACTS = Manifest.permission.READ_CONTACTS;

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		Permissions.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<Permissions> constructor = Permissions.class.getDeclaredConstructor();
		constructor.setAccessible(true);

		// Act:
		constructor.newInstance();
	}

	@Ignore("Test manifest is ignored!")
	@Test public void testHasAnyOf() {
		// Act + Assert:
		assertThat(Permissions.hasAnyOf(getContext(),
				INTERNET,
				WAKE_LOCK,
				WRITE_EXTERNAL_STORAGE,
				READ_CONTACTS
		), is(true));
		assertThat(Permissions.hasAnyOf(getContext(),
				WRITE_EXTERNAL_STORAGE,
				READ_CONTACTS
		), is(false));
	}

	@Ignore("Test manifest is ignored!")
	@Test public void testHasAllOf() {
		// Act + Assert:
		assertThat(Permissions.hasAllOf(getContext(),
				INTERNET,
				WAKE_LOCK
		), is(true));
		assertThat(Permissions.hasAllOf(getContext(),
				INTERNET,
				WAKE_LOCK,
				WRITE_EXTERNAL_STORAGE
		), is(false));
	}

	@Ignore("Test manifest is ignored!")
	@Test public void testHas() {
		// Act + Assert:
		assertThat(Permissions.has(getContext(), INTERNET), is(true));
		assertThat(Permissions.has(getContext(), WAKE_LOCK), is(true));
		assertThat(Permissions.has(getContext(), WRITE_EXTERNAL_STORAGE), is(false));
	}

	@Ignore("Test manifest is ignored!")
	@Test public void testIsSingleGranted() {
		// Act + Assert:
		assertThat(Permissions.isSingleGranted(new int[0]), is(false));
		assertThat(Permissions.isSingleGranted(new int[]{PackageManager.PERMISSION_DENIED}), is(false));
		assertThat(
				Permissions.isSingleGranted(new int[]{
						PackageManager.PERMISSION_DENIED,
						PackageManager.PERMISSION_GRANTED
				}),
				is(false)
		);
		assertThat(Permissions.isSingleGranted(new int[]{PackageManager.PERMISSION_GRANTED}), is(false));
	}
}