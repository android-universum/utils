/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import androidx.annotation.IntRange;

/**
 * This class represents a crate to store a set of integer flags instead of a boolean flags to improve
 * memory efficiency of an Android application.
 * <p>
 * <b>Note</b>, that usage of this class to store {@code int} flags instead of boolean flags is
 * efficient (in case of memory usage) only if you need to store and handle more than <b>4</b> boolean
 * flags. It's because, the size of this object will be "always" <b>16 bytes</b> and at the other hand
 * the size of each of boolean flag is <b>4 bytes</b> so if you have for example <b>8</b> booleans
 * to handle some states, these boolean fields will together use <b>24 bytes</b> of an available
 * memory.
 * <p>
 * Of course, significant memory savings can be visible only in case when this class is used within
 * the context of a class of which instances are in an android application used and instantiated in
 * large amount, like hundreds or thousands of instances and more.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class Flags {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "Flags";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Value holding multiple integer flags (binary shifted).
	 */
	private int flags;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of Flags with current flags initialized to {@code 0}.
	 */
	public Flags() {
		this(0);
	}

	/**
	 * Creates a new instance of Flags with the initial <var>flags</var> value.
	 *
	 * @param flags The initial value for flags.
	 */
	public Flags(@IntRange(from = 0, to = Integer.MAX_VALUE) final int flags) {
		this.flags = flags;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Adds the specified <var>flag</var> to the current ones (if not presented yet).
	 *
	 * @param flag The desired flag to add.
	 * @return This flags to allow methods chaining.
	 *
	 * @see #has(int)
	 * @see #get()
	 */
	public Flags add(@IntRange(from = 1, to = Integer.MAX_VALUE) final int flag) {
		this.flags |= flag;
		return this;
	}

	/**
	 * Removes the specified <var>flag</var> from the current ones (if presented).
	 *
	 * @param flag The desired flag to remove.
	 * @return This flags to allow methods chaining.
	 *
	 * @see #has(int)
	 * @see #get()
	 */
	public Flags remove(@IntRange(from = 1, to = Integer.MAX_VALUE) final int flag) {
		this.flags &= ~flag;
		return this;
	}

	/**
	 * Checks whether the requested <var>flag</var> is presented within the current flags or not.
	 *
	 * @param flag The desired flag to check.
	 * @return {@code True} if flag is presented, {@code false} otherwise.
	 * @see #add(int)
	 * @see #remove(int)
	 * @see #get()
	 */
	public boolean has(@IntRange(from = 1, to = Integer.MAX_VALUE) final int flag) {
		return (flags & flag) != 0;
	}

	/**
	 * Returns the current integer flags.
	 *
	 * @return Current flags.
	 */
	@IntRange(from = 0, to = Integer.MAX_VALUE) public int get() {
		return flags;
	}

	/**
	 * Resets value of this flags to {@code 0}.
	 *
	 * @return This flags to allow methods chaining.
	 */
	public Flags reset() {
		this.flags = 0;
		return this;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}