/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.os.Bundle;

import androidx.annotation.NonNull;

/**
 * Utility class that may be used to create keys for {@link Bundle} map.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class BundleKey {

	/**
	 */
	private BundleKey() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/**
	 * Creates basic bundle key.
	 *
	 * @param keyOwner Class which will "own" the key.
	 * @param keyName  Name of the key to be created.
	 * @return Key in format {@code 'fully.qualified.OwnerName.KEY.KeyName'}.
	 *
	 * @see #create(Class, String)
	 */
	@NonNull public static String basic(@NonNull final Class<?> keyOwner, @NonNull final String keyName) {
		return create(keyOwner, ".KEY." + keyName);
	}

	/**
	 * Creates a key intended to be used to identify a single argument passed via {@link Bundle}.
	 *
	 * @param keyOwner Class which will "own" the key.
	 * @param keyName  Name of the key to be created.
	 * @return Key in format {@code 'fully.qualified.OwnerName.ARGUMENT.KeyName'}.
	 *
	 * @see #create(Class, String)
	 */
	@NonNull public static String argument(@NonNull final Class<?> keyOwner, @NonNull final String keyName) {
		return create(keyOwner, ".ARGUMENT." + keyName);
	}

	/**
	 * Creates a key intended to be used to identify a single value persisted in {@link Bundle} as
	 * saved state.
	 *
	 * @param keyOwner Class which will "own" the key.
	 * @param keyName  Name of the key to be created.
	 * @return Key in format {@code 'fully.qualified.OwnerName.SAVED_STATE.KeyName'}.
	 *
	 * @see #create(Class, String)
	 */
	@NonNull public static String savedState(@NonNull final Class<?> keyOwner, @NonNull final String keyName) {
		return create(keyOwner, ".SAVED_STATE." + keyName);
	}

	/**
	 * Creates a new key with <var>keyName</var> for the specified <var>keyOwner</var> class.
	 *
	 * @param keyOwner Class which will "own" the key.
	 * @param keyName  Name of the key to be created.
	 * @return Key consisting of fully qualified name of the given owner class appended with key name.
	 */
	@NonNull public static String create(final Class<?> keyOwner, final String keyName) {
		return keyOwner.getName() + keyName;
	}
}