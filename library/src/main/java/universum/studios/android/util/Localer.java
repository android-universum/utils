/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.Locale;

import androidx.annotation.NonNull;

/**
 * The Localer class provides API allowing to set up custom {@link Locale} for an Android application.
 * Which locale should be used can be specified via {@link #setLocale(Locale, Resources)}.
 * The specified locale will be set up as default locale via {@link Locale#setDefault(Locale)} and
 * also will be used to update the configuration of the Android application resources so also strings
 * from resources will be properly obtained based on the specified locale.
 * <p>
 * The best place where to place Localer is within custom {@link android.app.Application} implementation
 * as shown in the following sample:
 * <pre>
 * public final class LocalizedApplication extends Application {
 *
 *      // Localer instance used to update locale of this Android application.
 *      private final Localer localer = new Localer(Locale.FRENCH);
 *
 *      &#64;Override
 *      public void onCreate() {
 *          super.onCreate();
 *          this.localer.dispatchApplicationCreated(getResources());
 *      }
 *
 *      &#64;Override
 *      public void onConfigurationChanged(Configuration newConfig) {
 *          super.onConfigurationChanged(newConfig);
 *          this.localer.dispatchConfigurationChanged(newConfig, getResources());
 *      }
 *
 *      // Changes the current application locale to the specified one.
 *      public void changeLocale(@NonNull Locale locale) {
 *          this.localer.setLocale(locale, getResources());
 *          // Note, that the call above will change default locale and also locale of this Android
 *          // application resources, unfortunately, if you want to this change be presented also in
 *          // the currently running UI (activity), such an UI needs to be recreated so a new values
 *          // from resources based on the new locale can be loaded properly.
 *      }
 * }
 * </pre>
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class Localer {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = " Localer";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * An instance of locale used as default locale for this Android application. All locale related
	 * stuffs will depends on this locale.
	 */
	private Locale locale;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of Localer to manage custom locale within this Android application with
	 * default Locale set to {@link Locale#ENGLISH}.
	 */
	public Localer() {
		this(Locale.ENGLISH);
	}

	/**
	 * Creates a new instance of Localer to manage custom locale within this Android application with
	 * the given default Locale.
	 *
	 * @param locale The desired locale.
	 * @see #setLocale(Locale, Resources)
	 */
	public Localer(@NonNull final Locale locale) {
		this.locale = locale;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Called from application's {@link android.app.Application#onCreate()} to dispatch, that application
	 * was just created.
	 *
	 * @param resources An application resources.
	 * @see #dispatchConfigurationChanged(Configuration, Resources)
	 */
	public void dispatchApplicationCreated(@NonNull final Resources resources) {
		this.updateLocale(resources.getConfiguration(), resources);
	}

	/**
	 * Called from application's {@link android.app.Application#onConfigurationChanged(Configuration)}
	 * to dispatch changed configuration.
	 *
	 * @param newConfig An instance of changed configuration.
	 * @param resources An application resources.
	 * @see #dispatchApplicationCreated(Resources)
	 */
	public void dispatchConfigurationChanged(@NonNull final Configuration newConfig, @NonNull final Resources resources) {
		this.updateLocale(new Configuration(newConfig), resources);
	}

	/**
	 * Sets the given locale to be used as locale for this Android application, so all stuffs related
	 * to locale, like resources will depends on the passed Locale.
	 * <p>
	 * <b>Note, that this does not reload the currently running UI (activity/-ies) so the locale
	 * change will be not visible in the UI until it is re-created so a new values from resources
	 * based on the changed locale can be properly loaded again.</b>
	 *
	 * @param locale    An instance of locale to be used as locale for this Android application.
	 * @param resources An application resources.
	 */
	public void setLocale(@NonNull final Locale locale, @NonNull final Resources resources) {
		this.locale = locale;
		this.updateLocale(resources.getConfiguration(), resources);
	}

	/**
	 * Returns the current locale assigned to this Localer.
	 *
	 * @return An instance of Locale or {@code null} if there is no locale assigned.
	 */
	@NonNull public Locale getLocale() {
		return locale;
	}

	/**
	 * Updates the given configuration with the current locale instance and also dispatches locale
	 * change to the passed <var>resources</var>.
	 *
	 * @param config    An instance of configuration where should be locale updated to the current one.
	 * @param resources An application resources.
	 */
	private void updateLocale(final Configuration config, final Resources resources) {
		// Set up default locale just for this application.
		Locale.setDefault(locale);
		// Update configuration locale.
		config.locale = locale;
		// Dispatch also to resources so any string related stuff will work properly.
		resources.updateConfiguration(config, resources.getDisplayMetrics());
	}

	/*
	 * Inner classes ===============================================================================
	 */
}