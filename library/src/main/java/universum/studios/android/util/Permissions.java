/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Process;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Size;

/**
 * Helper class that may be used for permissions checking. Check for a single permission can be done
 * via {@link #has(Context, String)} and for set of permissions can be used {@link #hasAllOf(Context, String...)}
 * or {@link #hasAnyOf(Context, String...)} methods.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public final class Permissions {

	/**
	 */
	private Permissions() {
		// Not allowed to be instantiated publicly.
		throw new UnsupportedOperationException();
	}

	/**
	 * Checks whether any of the specified <var>permissions</var> is granted for the current process
	 * and user id.
	 *
	 * @param context     Context used to check permission.
	 * @param permissions The desired set of permissions to check if they are granted.
	 * @return {@code True} if at least one of the specified permissions is granted, {@code false}
	 * otherwise.
	 * @see #has(Context, String)
	 * @see #hasAllOf(Context, String...)
	 */
	@CheckResult public static boolean hasAnyOf(@NonNull final Context context, @NonNull @Size(min = 1) final String... permissions) {
		for (final String permission : permissions) {
			if (has(context, permission)) return true;
		}
		return false;
	}

	/**
	 * Checks whether all of the specified <var>permissions</var> are granted for the current process
	 * and user id.
	 *
	 * @param context     Context used to check permission.
	 * @param permissions The desired set of permissions to check if they are granted.
	 * @return {@code True} if all of the specified permissions are granted, {@code false} otherwise.
	 * @see #has(Context, String)
	 * @see #hasAnyOf(Context, String...)
	 */
	@CheckResult public static boolean hasAllOf(@NonNull final Context context, @NonNull @Size(min = 1) final String... permissions) {
		for (final String permission : permissions) {
			if (!has(context, permission)) return false;
		}
		return true;
	}

	/**
	 * Checks whether the specified <var>permission</var> is granted for the current process and
	 * user id.
	 *
	 * @param context    Context used to check permission.
	 * @param permission The desired permission to check if it is granted.
	 * @return {@code True} if the specified permission is granted, {@code false} otherwise.
	 * @see Context#checkPermission(String, int, int)
	 * @see Manifest.permission
	 */
	@CheckResult public static boolean has(@NonNull final Context context, @NonNull final String permission) {
		return context.checkPermission(permission, Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED;
	}

	/**
	 * Checks whether the specified <var>grantResults</var> contain a single <b>granted</b> result.
	 * <p>
	 * If the given results array contains more than one result this check fails.
	 *
	 * @param grantResults The desired results to be checked.
	 * @return {@code True} if there is exactly one result with {@link PackageManager#PERMISSION_GRANTED}
	 * code, {@code false} otherwise.
	 */
	@CheckResult public static boolean isSingleGranted(@NonNull final int[] grantResults) {
		return grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
	}
}