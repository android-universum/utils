/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util.inner;

import android.app.Application;
import android.content.res.Configuration;

import java.util.Locale;

import androidx.annotation.NonNull;
import universum.studios.android.util.Localer;

/**
 * @author Martin Albedinsky
 */
public final class LocalizedApplication extends Application {

	private final Localer localer = new Localer(Locale.US);

	@Override public void onCreate() {
		super.onCreate();
		this.localer.dispatchApplicationCreated(getResources());
	}

	@Override public void onConfigurationChanged(@NonNull final Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		this.localer.dispatchConfigurationChanged(newConfig, getResources());
	}

	public void changeLocale(@NonNull final Locale locale) {
		this.localer.setLocale(locale, getResources());
	}
}