/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.Manifest;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class PermissionsTest extends AndroidTestCase {

	private static final String INTERNET = Manifest.permission.INTERNET;
	private static final String WAKE_LOCK = Manifest.permission.WAKE_LOCK;
	private static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
	private static final String READ_CONTACTS = Manifest.permission.READ_CONTACTS;

	@Test public void testHasAnyOf() {
		// Act + Assert:
		assertThat(Permissions.hasAnyOf(context(),
				INTERNET,
				WAKE_LOCK,
				WRITE_EXTERNAL_STORAGE,
				READ_CONTACTS
		), is(true));
		assertThat(Permissions.hasAnyOf(context(),
				WRITE_EXTERNAL_STORAGE,
				READ_CONTACTS
		), is(false));
	}

	@Test public void testHasAllOf() {
		// Act + Assert:
		assertThat(Permissions.hasAllOf(context(),
				INTERNET,
				WAKE_LOCK
		), is(true));
		assertThat(Permissions.hasAllOf(context(),
				INTERNET,
				WAKE_LOCK,
				WRITE_EXTERNAL_STORAGE
		), is(false));
	}

	@Test public void testHas() {
		// Act + Assert:
		assertThat(Permissions.has(context(), INTERNET), is(true));
		assertThat(Permissions.has(context(), WAKE_LOCK), is(true));
		assertThat(Permissions.has(context(), WRITE_EXTERNAL_STORAGE), is(false));
	}
}