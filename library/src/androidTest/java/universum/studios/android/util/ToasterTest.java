/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.widget.Toast;

import org.junit.Rule;
import org.junit.Test;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.test.TestActivity;
import universum.studios.android.util.test.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.not;

/**
 * @author Martin Albedinsky
 */
public final class ToasterTest extends AndroidTestCase {

	private static final String TOAST_TEXT = "Text within toast";

	@Rule public final ActivityTestRule<TestActivity> ACTIVITY_TEST_RULE = new ActivityTestRule<>(TestActivity.class);

	private TestActivity activity;

	@Override public void beforeTest() {
		super.beforeTest();
		this.activity = ACTIVITY_TEST_RULE.getActivity();
	}

	@Test public void showToastText() {
		showToastAndAssertThatItIsDisplayedWithText(() -> Toaster.showToast(activity, TOAST_TEXT), TOAST_TEXT);
	}

	@Test public void showToastTextDuration() {
		showToastAndAssertThatItIsDisplayedWithText(() -> Toaster.showToast(activity, TOAST_TEXT, Toast.LENGTH_SHORT), TOAST_TEXT);
	}

	@Test public void showToastResId() {
		showToastAndAssertThatItIsDisplayedWithText(() -> Toaster.showToast(activity, R.string.test_toast_text), context().getString(R.string.test_toast_text));
	}

	@Test public void showToastResIdWithArgs() {
		showToastAndAssertThatItIsDisplayedWithText(() -> Toaster.showToast(
				activity,
				Toast.LENGTH_SHORT,
				R.string.test_toast_text_with_args,
				"Large"
		), activity.getString(R.string.test_toast_text_with_args, "Large"));
	}

	@Test public void showToastTextWithArgs() {
		showToastAndAssertThatItIsDisplayedWithText(() -> {
			Toaster.showToast(activity, Toast.LENGTH_LONG, "%d items has been removed", 10);
			try {
				// Wait a while so the longer showing toast is properly dismissed.
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}, "10 items has been removed");
	}

	private void showToastAndAssertThatItIsDisplayedWithText(final Runnable toastRunnable, final String text) {
		// Arrange:
		try {
			// Wait a while so the previous toast is properly dismissed.
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Act:
		this.activity.runOnUiThread(toastRunnable);
		InstrumentationRegistry.getInstrumentation().waitForIdleSync();
		// Assert:
		assertToastIsDisplayedWithText(text);
	}

	private void assertToastIsDisplayedWithText(final String text) {
		onView(withText(text))
				.inRoot(withDecorView(not(activity.getWindow().getDecorView())))
				.check(matches(isDisplayed()));
	}
}