/*
 * *************************************************************************************************
 *                                 Copyright 2016 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.util;

import android.content.res.Resources;

import org.junit.Test;

import java.util.Locale;

import universum.studios.android.test.AndroidTestCase;
import universum.studios.android.util.inner.LocalizedApplication;
import universum.studios.android.util.test.R;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class LocalerTest extends AndroidTestCase {

	private static final int LOCALIZED_TEXT_RES = R.string.test_localer_message;
	private static final int LOCALIZED_PLURAL_RES = R.plurals.test_localer_plural;

	private static final Locale LOCALE_SLOVAK = new Locale("sk");

	private LocalizedApplication application;
	private Resources resources;

	@Override public void beforeTest() {
		super.beforeTest();
		this.application = (LocalizedApplication) context().getApplicationContext();
		this.resources = application.getResources();
	}

	@Test public void testLocalizedStrings() {
		// ENGLISH:
		application.changeLocale(Locale.ENGLISH);
		assertThat(resources.getString(LOCALIZED_TEXT_RES), is("Hello Localer test!"));
		// SLOVAK:
		application.changeLocale(LOCALE_SLOVAK);
		assertThat(resources.getString(LOCALIZED_TEXT_RES), is("Ahoj Localer test!"));
		// GERMAN:
		application.changeLocale(Locale.GERMAN);
		assertThat(resources.getString(LOCALIZED_TEXT_RES), is("Hallo Localer Test!"));
		// FRENCH:
		application.changeLocale(Locale.FRENCH);
		assertThat(resources.getString(LOCALIZED_TEXT_RES), is("Bonjour Localer essai!"));
	}

	@Test public void testLocalizedQuantityStrings() {
		// ENGLISH:
		application.changeLocale(Locale.ENGLISH);
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 0, 0), is("0 items"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 1, 1), is("one item"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 2, 2), is("2 items"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 100, 100), is("100 items"));
		// SLOVAK:
		application.changeLocale(LOCALE_SLOVAK);
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 0, 0), is("0 položiek"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 1, 1), is("jedna položka"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 2, 2), is("2 položky"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 5, 5), is("5 položiek"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 100, 100), is("100 položiek"));
		// GERMAN:
		application.changeLocale(Locale.GERMAN);
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 1, 1), is("ein Einzelteil"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 2, 2), is("2 Gegenstände"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 3, 3), is("3 Gegenstände"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 100, 100), is("100 Gegenstände"));
		// FRENCH:
		application.changeLocale(Locale.FRENCH);
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 0, 0), is("0 article"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 1, 1), is("1 article"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 2, 2), is("2 articles"));
		assertThat(resources.getQuantityString(LOCALIZED_PLURAL_RES, 100, 100), is("100 articles"));
	}
}